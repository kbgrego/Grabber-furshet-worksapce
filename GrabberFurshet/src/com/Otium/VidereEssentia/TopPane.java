package com.Otium.VidereEssentia;

import com.Otium.VidereEssentia.controls.ExitButton;
import com.Otium.VidereEssentia.controls.MinimizeButton;
import com.Otium.VidereEssentia.controls.StylerConsole;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

@SuppressWarnings("serial")
class TopPane extends JLabel implements StylerConsole{
	private JFrame control;
	private boolean press; 
	private Point mouseP;
	
	TopPane(String text, int width, int height){
		this(text,width,height,true);
	}
	
	TopPane(String text, int width, int height, boolean needExit){
		super(text, CENTER);
		setBounds(0, 0, width, height);
		setPreferredSize(new Dimension(width, height));
		setOpaque(true);
		
		setLayout(new BorderLayout());
		
		setBackground(TOPPANE_color);
		setForeground(FORE_color);
		
		setFont(new Font(FONT_style, Font.PLAIN, FONT_size));
		
		setVerticalAlignment(CENTER);
		
		add(new MinimizeButton(width), BorderLayout.WEST);
		
		if(needExit)		
			add(new ExitButton(width), BorderLayout.EAST);
		
		
		TopPane pane = this;
        addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(MouseEvent evt) {
                pane.setForeground(Color.white);
                setBackground(TOPPANE_HOVER_color);
            }

            public void mouseExited(MouseEvent evt) {
                pane.setForeground(FORE_color);
                setBackground(TOPPANE_color);
            }
            
            public void mousePressed(MouseEvent evt){
            	setBackground(TOPPANE_Click_color);
            	mouseP = evt.getLocationOnScreen();
            	press=true;
            	
            	if(control==null)
            		control = (JFrame) SwingUtilities.getRoot(pane);
            	
            	
            	if(evt.getClickCount()==2 && (control.getExtendedState()&Frame.MAXIMIZED_BOTH)==0 && needExit) 
            		control.setExtendedState(Frame.MAXIMIZED_BOTH);
            	else if(evt.getClickCount()==2 && (control.getExtendedState()&Frame.MAXIMIZED_BOTH)!=0)
            		control.setExtendedState(Frame.NORMAL);
            	
            }
            
            public void mouseReleased(MouseEvent evt){
            	setBackground(TOPPANE_HOVER_color);
            	press=false;
            }                    	            
        });
        
        addMouseMotionListener(new MouseMotionListener(){			
			@Override
			public void mouseMoved(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
            public void mouseDragged(MouseEvent evt){
            	if(press&&control!=null){            		
            		int pX = mouseP.x, pY = mouseP.y, nX = evt.getXOnScreen(), nY = evt.getYOnScreen(),
            			cX = control.getLocation().x,
            			cY = control.getLocation().y;
            		
            		if(cX==0 && cY==0 && ((control.getExtendedState()&JFrame.MAXIMIZED_BOTH)!=0) && (((nY - pY)!=0 || (nX - pX)!=0))){
            			control.setExtendedState(JFrame.NORMAL);
        				control.setLocation(nX - control.getPreferredSize().width/2, nY - 10);
        				mouseP = evt.getLocationOnScreen();
        				return;
            		}
            		
            		if((cX==0 || cX==control.getMaximumSize().width/2) && 
            		   (cY==0 || cY==control.getMaximumSize().height/2) && 
            		   (control.getWidth()==control.getMaximumSize().width ||
            		    control.getHeight()==control.getMaximumSize().height) && 
            		    (((nY - pY)!=0 || (nX - pX)!=0))){
            				control.setSize(control.getPreferredSize());
            				control.setLocation(nX - control.getPreferredSize().width/2, nY - 10);
            				mouseP = evt.getLocationOnScreen();
            				return;
            		}
            		
            		control.setLocation(cX + (nX - pX), cY + (nY - pY));
            		mouseP = evt.getLocationOnScreen();
            		
            		if(nY == 0 && needExit) {
            			control.setExtendedState(JFrame.MAXIMIZED_BOTH);
            			press=false;
            		}
            		
            		if(nY == control.getMaximumSize().height-1){
            			control.setLocation(0,control.getMaximumSize().height / 2);
            			control.setSize(control.getMaximumSize().width, control.getMaximumSize().height / 2);
            			control.setExtendedState(JFrame.MAXIMIZED_HORIZ);
            			press=false;            			
            		}
            		
            		if(nX == 0 && needExit){
            			control.setLocation(0,0);
            			control.setSize(control.getMaximumSize().width / 2, control.getMaximumSize().height);
            			control.setExtendedState(JFrame.MAXIMIZED_VERT);
            			press=false;
            		}
            		
            		if(nX == control.getMaximumSize().width-1){
            			control.setLocation(control.getMaximumSize().width / 2,0);
            			control.setSize(control.getMaximumSize().width / 2, control.getMaximumSize().height);
            			control.setExtendedState(JFrame.MAXIMIZED_VERT);
            			press=false;            			
            		}
            	}
            }
		});
	}
}
