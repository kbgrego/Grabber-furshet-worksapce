package com.Otium.VidereEssentia;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Tester {
	public static void main(String[] args) {
		Screen screen = new Screen("Tester", 800, 650);
		StringProperty sp = new SimpleStringProperty();
		sp.addListener(i -> System.out.println(i));		
		screen.readPassword(sp);
	}
	
}
