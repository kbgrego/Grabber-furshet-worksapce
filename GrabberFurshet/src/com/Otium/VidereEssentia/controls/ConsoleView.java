package com.Otium.VidereEssentia.controls;

import javax.swing.text.Style;
import javax.swing.text.StyledDocument;

public interface ConsoleView {
	public String[] nameStyles();
	public void setStyle(Style style, int index);
	public void showIn(StyledDocument doc);
}
