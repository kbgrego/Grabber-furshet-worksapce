package com.Otium.VidereEssentia.controls;

import javax.swing.*;
import java.awt.*;

@SuppressWarnings("serial")
public class ExitButton extends JButton implements StylerConsole {
	
	public ExitButton(int width){
        setText("Exit");
        setMinimumSize(new Dimension(80, 25));
        setBackground(TOPPANE_color);
        setBorderPainted(false);
        setFocusPainted(false);
        setContentAreaFilled(false);
        setForeground(FORE_color);
		setOpaque(true);
        setFont(new Font(FONT_style, Font.PLAIN, FONT_size));
        
        addActionListener((action) -> System.exit(0));
        
        ExitButton jButton = this;
        addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jButton.setForeground(Color.WHITE);
                jButton.setBackground(TOPPANE_HOVER_color);
            }

            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButton.setForeground(FORE_color);
                jButton.setBackground(TOPPANE_color);
            }
        });
	}

}
