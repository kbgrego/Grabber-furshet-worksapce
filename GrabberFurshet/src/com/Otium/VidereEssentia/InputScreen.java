package com.Otium.VidereEssentia;

import com.Otium.VidereEssentia.controls.StylerConsole;
import javafx.beans.property.StringProperty;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

@SuppressWarnings("serial")
public class InputScreen extends JFrame implements StylerConsole {
	private final JLabel lStatus;
	private final JTextField tInput;
	public InputScreen(StringProperty String){
		super("Input");		
        setSize(400,200);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation((screenSize.width - 400) / 2, (screenSize.height - 200) / 2);
        setMaximumSize(screenSize);
        setMinimumSize(new Dimension(400, 200));
        setLocationRelativeTo(null);      
        
        setUndecorated(true);
        
        setLayout(new BorderLayout());
        
        JPanel form = new JPanel();
        form.setBackground(GROUND_color);
        form.setBorder(new EmptyBorder(50, 50, 50, 50));
        form.setLayout(new FlowLayout());
        
        tInput = new JTextField("", 21);
		tInput.setForeground(Color.white);		
		tInput.setFont(new Font(FONT_style, Font.PLAIN, FONT_size));
		tInput.setBackground(TOPPANE_color);
		tInput.setBorder(null);
		tInput.setCaretColor(FORE_color);		
		
		tInput.addKeyListener(new KeyListener() {			
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode()==KeyEvent.VK_ENTER){
					String.set(tInput.getText());
				}
			}

			public void keyTyped(KeyEvent e) {	}

			public void keyReleased(KeyEvent e) {	}
		});
		
		form.add(tInput);		
        
        lStatus = new JLabel("", JLabel.CENTER);
		lStatus.setForeground(FORE_color);
		lStatus.setBackground(GROUND_color);
		lStatus.setBorder(new EmptyBorder(10, 10, 10, 10));
		lStatus.setOpaque(true);
		lStatus.setFont(new Font(FONT_style, Font.PLAIN, FONT_size));
		lStatus.setSize(400, 70);
		
        Container contentPane = getContentPane();
        contentPane.add(new TopPane("Input", 400, 25, false), BorderLayout.NORTH);
        contentPane.add(lStatus, BorderLayout.CENTER);
        contentPane.add(form, BorderLayout.SOUTH);
		
        //pack();
		
        setVisible( true );                        
	}
	
	public void setStatus(String status){
		lStatus.setText(status);
		pack();
		
	}

	public void reset() {
		tInput.setText("");
		tInput.requestFocus();
	}
}
