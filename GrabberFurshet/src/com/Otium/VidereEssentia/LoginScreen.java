package com.Otium.VidereEssentia;

import com.Otium.VidereEssentia.controls.StylerConsole;
import javafx.beans.property.StringProperty;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

@SuppressWarnings("serial")
public class LoginScreen extends JFrame implements StylerConsole {
	private final JLabel lStatus;
	private final JTextField tLogin;
	private final JPasswordField tPassword;
	public LoginScreen(StringProperty login, StringProperty pass){
		super("Login");		
        setSize(400,200);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation((screenSize.width - 400) / 2, (screenSize.height - 200) / 2);
        setMaximumSize(screenSize);
        setMinimumSize(new Dimension(400, 200));
        setLocationRelativeTo(null);        
        setUndecorated(true);  
        
        setLayout(new BorderLayout());
        
        JPanel form = new JPanel();
        form.setBackground(GROUND_color);
        form.setBorder(new EmptyBorder(25, 50, 50, 50));
        form.setLayout(new FlowLayout());
        
        JPanel row1 = new JPanel();   
        row1.setBackground(GROUND_color);
        row1.setLayout(new BoxLayout(row1, BoxLayout.PAGE_AXIS));        
        
		JLabel lLogin = new JLabel("login", JLabel.RIGHT);
		lLogin.setForeground(FORE_color);
		lLogin.setFont(new Font(FONT_style, Font.PLAIN, FONT_size));
		
		JLabel lpass = new JLabel("password", JLabel.RIGHT);
		lpass.setForeground(FORE_color);
		lpass.setFont(new Font(FONT_style, Font.PLAIN, FONT_size));
			
		row1.add(lLogin);
		row1.add(lpass);
		
        JPanel row2 = new JPanel();
        row2.setLayout(new BoxLayout(row2, BoxLayout.PAGE_AXIS)); 
        
        tLogin = new JTextField("", 20);
		tLogin.setForeground(Color.white);		
		tLogin.setFont(new Font(FONT_style, Font.PLAIN, FONT_size));
		tLogin.setBackground(GROUND_color);
		tLogin.setBorder(null);
		tLogin.setCaretColor(FORE_color);		
        
		tPassword = new JPasswordField("", 20);
		tPassword.setForeground(Color.white);		
		tPassword.setFont(new Font(FONT_style, Font.PLAIN, FONT_size));
		tPassword.setBackground(GROUND_color);
		tPassword.setBorder(null);
		tPassword.setCaretColor(FORE_color);
		
		tLogin.addKeyListener(new KeyListener() {			
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode()==KeyEvent.VK_ENTER){
					login.set(tLogin.getText());
					pass.set(new String(tPassword.getPassword()));
					tPassword.requestFocus();
				}
			}

			public void keyTyped(KeyEvent e) {	}

			public void keyReleased(KeyEvent e) {	}
		});
				
		tPassword.addKeyListener(new KeyListener() {			
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode()==KeyEvent.VK_ENTER){
					login.set(tLogin.getText());
					pass.set(new String(tPassword.getPassword()));
				}
			}

			public void keyTyped(KeyEvent e) {	}

			public void keyReleased(KeyEvent e) {	}
		});
		
		row2.add(tLogin);
		row2.add(tPassword);
		
		form.add(row1);
		form.add(row2);		
        
        lStatus = new JLabel("", JLabel.CENTER);
		lStatus.setForeground(FORE_color);
		lStatus.setBackground(GROUND_color);
		lStatus.setBorder(new EmptyBorder(10, 10, 10, 10));
		lStatus.setOpaque(true);
		lStatus.setFont(new Font(FONT_style, Font.PLAIN, FONT_size));
		
        Container contentPane = getContentPane();
        contentPane.add(new TopPane("Login", 400, 25, false), BorderLayout.NORTH);
        contentPane.add(lStatus, BorderLayout.CENTER);
        contentPane.add(form, BorderLayout.SOUTH);
		
        //pack();
		
        setVisible( true );                        
	}
	
	public void setStatus(String status){
		lStatus.setText(status);
	}

	public void reset() {
		tLogin.setText("");
		tPassword.setText("");
		tLogin.requestFocus();
	}
}
