package com.Otium.VidereEssentia;

import com.Otium.VidereEssentia.controls.ConsoleView;
import com.Otium.VidereEssentia.controls.StylerConsole;
import javafx.beans.property.StringProperty;

import javax.swing.*;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;


@SuppressWarnings("serial")
class MainConsole extends JScrollPane implements StylerConsole {
	private  JTextPane Pane       = new JTextPane(); 
    private  Style     heading    = null; // ����� ���������
    private  Style     normal     = null; // ����� ������

	MainConsole(int width, int height){
		super();
		Pane=new JTextPane();
		setViewportView(Pane);
		setBorder(null);
		setBounds(0, 25, width, height - 25);
		Pane.setBackground(GROUND_color);
		Pane.setEditable(false);
        
        normal = Pane.addStyle(STYLE_normal, null);
        StyleConstants.setFontFamily(normal, FONT_style); 
        StyleConstants.setFontSize(normal, FONT_size);
        StyleConstants.setForeground(normal, FORE_color);
        
        heading = Pane.addStyle(StylerConsole.STYLE_heading, normal);
        StyleConstants.setFontSize(heading, FONT_size);
        StyleConstants.setBold(heading, true);
	}

	public void setView(ConsoleView view) {
		int i=0;
		for(String name : view.nameStyles())
			view.setStyle(Pane.addStyle(name, null), i++);
		view.showIn(Pane.getStyledDocument());		
	}

	public void clear() {
		Pane.setText("");
		Pane.setParagraphAttributes(normal, true);
	}	
	
	private void echoIn(char keyChar) {
		Document doc = Pane.getDocument();
		try {
			doc.insertString(doc.getLength(), Character.toString(keyChar), normal);
		} catch (BadLocationException e) {

		}			
	}
	
	public void echoIn(String string){
		Document doc = Pane.getDocument();
		try {
			doc.insertString(doc.getLength(), string, normal);
		} catch (BadLocationException e) {

		}	
	}
	
	private void removeLastChar(){
		Document doc = Pane.getDocument();
		try {
			doc.remove(doc.getLength()-1, 1);
		} catch (BadLocationException e) {

		}		
	}

	public void replace(String string) {
		Document doc = Pane.getDocument();
		int remove=doc.getLength();
		try {
			doc.insertString(0, string, normal);
			doc.remove(remove, remove);
		} catch (BadLocationException e) {
			e.printStackTrace();
		}		
	}
	

	public void readLine(StringProperty readLine){
		Pane.addKeyListener(new KeyListener() {
			String line="";
			@Override
			public void keyTyped(KeyEvent e) {
				if(e.getKeyChar()=='\n'){
					readLine.set(line);
					line=null;
					Pane.removeKeyListener(this);
				} else if (e.getKeyChar()=='\b' && line.length()>1){
					line = line.substring(0, line.length()-1);
					removeLastChar();
				} else if (e.getKeyChar()=='\b' && line.length()==1){
					line = "";
					removeLastChar();
				} else if(e.getKeyChar()!='\b'){
					echoIn(e.getKeyChar());
					line = line + e.getKeyChar();
				}			
			}	

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
	}
	
	public void readPassword(StringProperty readLine){
		Pane.addKeyListener(new KeyListener() {
			String line="";
			@Override
			public void keyTyped(KeyEvent e) {
				if(e.getKeyChar()=='\n'){
					readLine.set(line);
					line=null;
					Pane.removeKeyListener(this);
				} else if (e.getKeyChar()=='\b' && line.length()>1){
					line = line.substring(0, line.length()-1);
					removeLastChar();
				} else if (e.getKeyChar()=='\b' && line.length()==1){
					line = "";
					removeLastChar();
				} else if(e.getKeyChar()!='\b'){
					echoIn((char)42);
					line = line + e.getKeyChar();
				}
				System.out.println(line);
			}	

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
	}
}
