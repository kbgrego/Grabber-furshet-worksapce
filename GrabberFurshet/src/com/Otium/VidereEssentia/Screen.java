package com.Otium.VidereEssentia;

import com.Otium.VidereEssentia.controls.FileLogo;
import com.Otium.VidereEssentia.controls.Logo;
import com.Otium.VidereEssentia.controls.StylerConsole;
import javafx.beans.property.StringProperty;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class Screen extends JFrame implements StylerConsole {
	private static final List<Screen> OpenedScreens = new ArrayList<>();
	
	private static void OrganazeWindows(){
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		switch(OpenedScreens.size()){
			case 1: OpenedScreens.get(0).setSize(screenSize);
			        OpenedScreens.get(0).setLocation(0, 0);
			        break;
			case 2: for(int i=0;i<OpenedScreens.size();i++){
						OpenedScreens.get(i).setSize(screenSize.width/2, screenSize.height);
						OpenedScreens.get(i).setLocation(i*(screenSize.width/2),0);
					}
			        break;
		}
	}
	
	JPanel panel;
	Component VIEW;
	
	public Screen(String title, int width, int height) {
		this(title, new VELogo(), width, height);
	}
	

	public Screen(String title, Logo logo, int width, int height) {
		super(title);
		OpenedScreens.add(this);
        //setSize(width,height);
        if(OpenedScreens.size()==1)
        	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        //setLocation((screenSize.width - width) / 2, (screenSize.height - height) / 2);
        setMaximumSize(screenSize);
        setPreferredSize(new Dimension(width, height));
        setLocationRelativeTo(null);
        setBackground(GROUND_color);
        setLayout(new BorderLayout());
        //setSize(getMaximumSize());
        //setLocation(0, 0);        
        
        setUndecorated(true);     

        add( new TopPane(title,width,25), BorderLayout.NORTH );
        
        MainConsole console = new MainConsole(width,height);
        
        setView(console);       
        
        setVisible( true );  
        if(logo!=null)	{
        	console.setView(logo);
        	try { Thread.sleep(3000); } catch (InterruptedException e) {}
        }
//        console.clear();
		OrganazeWindows();
	}
	
	public void setImage(Image image){
		setIconImage(image);
	}
	
	public void setView(Component view){
		if(VIEW!=null)
			remove(VIEW);
		add(view, BorderLayout.CENTER);
		VIEW = view;
	}

	public void echoIn(String string) {
		if(VIEW instanceof MainConsole){
			MainConsole console = (MainConsole) VIEW;
			console.echoIn(string);
		}
	}

	public void echo(String string) {
		if(VIEW instanceof MainConsole){
			MainConsole console = (MainConsole) VIEW;
			console.echoIn(string);
			console.echoIn("\n");
		}
	}


	public void printLogo(FileLogo fileLogo) {		
		if(VIEW instanceof MainConsole){
			MainConsole console = (MainConsole) VIEW;
			console.clear();
			console.setView(fileLogo);
		}		
	}


	public void replace(String string) {
		if(VIEW instanceof MainConsole){
			MainConsole console = (MainConsole) VIEW;
			console.replace(string);
		}
	}

	public void clear() {
		if(VIEW instanceof MainConsole){
			MainConsole console = (MainConsole) VIEW;
			console.clear();
		}
	}


	public void readLine(StringProperty sp) {
		if(VIEW instanceof MainConsole){
			MainConsole console = (MainConsole) VIEW;
			console.readLine(sp);
		}		
	}


	public void readPassword(StringProperty sp) {
		if(VIEW instanceof MainConsole){
			MainConsole console = (MainConsole) VIEW;
			console.readPassword(sp);
		}	
	}

	/*
	int posLastLine;
	String LastLine;
	public String readLine(){
		posLastLine = console.getLineCount()-1;
		try {
			LastLine = console.getText(console.getLineStartOffset(posLastLine), console.getLineEndOffset(posLastLine));
		} catch (BadLocationException e) {
		}
        console.getDocument().addDocumentListener(getDocumentListner());
        console.addCaretListener(getCaretListner());
        console.setEditable(true);

        return console.getText(offs, len);
	}
	
	private CaretListener getCaretListner() {

		return  new CaretListener() {
			@Override
		      public void caretUpdate(CaretEvent caretEvent) {
				if(caretEvent.getDot()!=caretEvent.getMark())
					console.setSelectionStart(caretEvent.getDot());
	            try {
		          if(console.getLineOfOffset(caretEvent.getDot())!=console.getLineCount()-1)
		      			console.moveCaretPosition(console.getLineEndOffset(console.getLineCount()-1));
	      		} catch (BadLocationException e) {
	      			e.printStackTrace();  	        	  
		        }
			}
		};
	}

	private DocumentListener getDocumentListner(){
		return new DocumentListener() {

            @Override
            public void removeUpdate(DocumentEvent e) {
            	//System.out.println(console.getText());
            }

            @Override
            public void insertUpdate(DocumentEvent e) {
            	//System.out.println(console.getText());
            }

            @Override
            public void changedUpdate(DocumentEvent arg0) {
            	//System.out.println(console.getText());
            }
        };
	}
	*/
}
