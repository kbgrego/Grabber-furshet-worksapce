package com.Otium.Grabber.Furshet;

import com.Otium.VidereEssentia.Screen;

class Timer extends Thread implements Runnable{
	private boolean trigger;
	private Thread mainTread;
	private Screen screen;

	Timer(Thread main, Screen screen){			
		trigger = false;
		mainTread = main;
		this.screen = screen;
		this.start();
	}
	
	public void setEnd(){
		trigger = true;
	}
	
	
	public void run(){		
		while(true){
			screen.echoIn(" .");
			try {sleep(1000);} catch (Exception e) {}
			if(trigger || !mainTread.isAlive() )break;						
		}		
	}
}
