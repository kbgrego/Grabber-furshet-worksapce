package com.Otium.Grabber.Furshet;

import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.jsoup.nodes.*;

import com.Otium.Grabber.model.GrabberFactory;
import com.Otium.VidereEssentia.InputScreen;
import com.Otium.VidereEssentia.LoginScreen;
import com.Otium.VidereEssentia.Screen;
import com.Otium.VidereEssentia.controls.FileLogo;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Furshet extends GrabberFactory {
	private final static DateTimeFormatter Formatter = DateTimeFormatter.ofPattern("d.MM.yyyy");
	private final static DateTimeFormatter Formatter_2 = DateTimeFormatter.ofPattern("dd.MM.yyyy");
	private static String login="";
	private static String password="";

	private static String restFile;
	
	private static LocalDate bdate;
	private static LocalDate edate;
	
	static Map<String, String[]> Shops;
	
	static Screen Screen;
	
	static Integer ended=0;
	
	public Furshet() throws IOException {
		super("https://supply.furshet.ua/");
		Screen = new Screen("Furshet", new FileLogo("Logo.lg"), 1200, 600);
				
		Date date = new Date();

		restFile = "�������_" + Formatter_2.format(LocalDate.now()) + ".csv";

//		new FileLogo("Logo.lg").getLogoStrings().length
//		Screen.echo();
			
  	    SignIN();	    
	    getPeriod();
	    Screen.clear();
		buildMapShops();
//        Excel.openSchameFileRests();
		getRests();		
		
//		while(ended!=1 /* Shops.size() */ ){
//			try {
//				Thread.sleep(1000);
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//			}
//		}
//
//		Excel.saveFileRests();
		
		printFooter(new Date().getTime()-date.getTime());		
	}
	
	
	
	synchronized private void SignIN() {	
		login="";
		password="";
		
		StringProperty s_login = new SimpleStringProperty("");
		StringProperty s_pass = new SimpleStringProperty("");
		s_login.addListener((a,b,c) ->{synchronized (this){notify();}});
		s_pass.addListener((a,b,c) -> {synchronized (this){notify();}});
		
		LoginScreen login_form = new LoginScreen(s_login,s_pass);
		login_form.setStatus("���� � �������");
		

		
		while(true){		    
		    try {
				wait();
		    	checkLoginPassword(s_login.get(), s_pass.get());
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		    
		    if(!login.isEmpty()&&!password.isEmpty()){
		    	login_form.setStatus("�����������");
			    getSessionID();			    
			    if(tryLogin()) {
			    	login_form.dispose();
			    	break;		
			    } else {
					login = "";
					password = "";			    	
			    	login_form.setStatus("���� �� ������, ���������� ��� ���.");
			    	login_form.reset();
			    }
		    }
		}
		login_form.dispose();
	}
	
	private void checkLoginPassword(String login, String pass){
		if(!login.isEmpty()&&!pass.isEmpty()){
			Furshet.login = login;
			Furshet.password = pass;
		}
	}

	private void getSessionID() {
		Screen.echo("����������� � �����  furshet.ua");
		getPageHTML("");
	}



	private boolean tryLogin() {
		String error="";
		Timer timer = new Timer(Thread.currentThread(), Screen);
		Screen.echoIn("����������� � �������");
		try{
			postPageHTML("", "AUTH_FORM=Y&TYPE=AUTH&backurl=%2F&USER_LOGIN=" + login + "&USER_PASSWORD=" + password + "&Login=%C2%EE%E9%F2%E8");
			timer.setEnd();
			Thread.sleep(1000);
			if(document.htmlDoc.getElementsByClass("errortext").size() > 0) 
				error = document.htmlDoc.getElementsByClass("errortext").get(0).childNodes().get(0).outerHtml();
		} catch (Exception e) {
			Screen.echo("������");
			Screen.echo("������� ����������� ����� ��� ��������� ������ �����������.");
			return false;
		}		
		
		if(error.equals("")){
			Screen.echo(" ���������");
			return true;
		} else {
			Screen.echo(" ������");
			Screen.echo(error);			 
			return false;
		}
	}
	
	@SuppressWarnings("resource")
	synchronized private void getPeriod() {
		StringProperty period = new SimpleStringProperty("");
		period.addListener((a,b,c) -> {synchronized (this) {notify();}});
		InputScreen input = new InputScreen(period);		
		input.setStatus("������� ������ ��������");
		
		while(true){
			
			try { wait(); } catch (InterruptedException e) {		}
			
			if(!period.get().equals("")){
				if(period.get().indexOf('-')==0 || period.get().split("-").length>2){
					input.setStatus("������� ��� ���� ����� �����.");
				}					
				try {
					bdate = LocalDate.parse(period.get().split("-")[0].trim(),Formatter);
					edate = LocalDate.parse(period.get().split("-")[1].trim(),Formatter);
					
					if(edate.isBefore(bdate)){
						input.setStatus("�������� ���� �� ����� ���� ������ ���������.");
						continue;
					}
					
					if(edate.isAfter(bdate.plusMonths(1))){
						input.setStatus("������ ����� ���� ������ ������ ������ ������.");
						continue;
					}
						
				} catch (Exception e){
					Screen.echo("������� ���� ����� �����. ���� �������� ����� (����.�����.���).");
					continue;
				}
				Screen.echo(bdate.format( Formatter));
				Screen.echo(edate.format( Formatter));
				break;
			}				
		}
		input.dispose();
	}

	private void printFooter(long diff) {
		long diffMiliSeconds = diff % 1000;
		long diffSeconds = diff / 1000 % 60;
		long diffMinutes = diff / (60 * 1000) % 60;
		long diffHours = diff / (60 * 60 * 1000);
		
		String duration_s = String.format("%02d:%02d:%02d.%03d", diffHours, diffMinutes, diffSeconds, diffMiliSeconds);
		
		Screen.echo("������� ��������� �� " + duration_s.toString());
		//Screen.printLogo(new FileLogo("Logo_EXIT.lg"));
	}

	private void getRests() throws IOException {

		ThreadScreenUpdate t = new ThreadScreenUpdate();
		Timer timer = new Timer(Thread.currentThread(), Screen);
		for(String shop : Shops.keySet()) {
			int count=1;
			//System.out.println("�������� �������� " + "(" + count++ +") �������� " + Shops.get(shop)[0]);
			/*			
			new ThreadGetShopRests(shop);
			try {
				while(count%1==count/1 && count/1!=ended)
					Thread.sleep(30000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}*/
			
			Shops.get(shop)[1] = "��������";
			//updateScreen(screen);
			Screen.echoIn("�������� �������� " + "(" + (ended+1) +") ��������  - " + Shops.get(shop)[0]);
			do {
				getRestPage(shop);
			} while (Shops.get(shop)[1] == "������" && count++ < 5);
			Screen.echoIn(" ���������� ����� " + Shops.get(shop)[4]);
            Screen.echo(" OK");
			Shops.get(shop)[1] = "������";
			//Screen.echo("������");
			ended++;
		}
		t.setEnd();
		timer.setEnd();
	}
	
	private class ThreadScreenUpdate extends Thread implements Runnable {
		private boolean end;
		ThreadScreenUpdate(){
			end = false;
			this.start();
		}
		
		public void run(){
			while(true){
				try {
					updateScreen();
					if(end)break;
					sleep(1000);	
				} catch (Exception e) {
					
				}
			}
		}
		
		void setEnd(){
			end = true;
		}
		
	}
	
	private class ThreadGetShopRests extends Thread implements Runnable {
		String rid_shop;
		ThreadGetShopRests(String rid_shop){
			this.start();
			this.rid_shop = rid_shop;
		}
		
		public void run() {


			try {
				getRestPage(rid_shop);
			} catch (IOException e) {
				e.printStackTrace();
			}

			Screen.echo("��������� ������� �������� " + Shops.get(rid_shop)[0]);
		}
		
		
		public void destroy(){

			ended++;
		}
	}

	private void updateScreen() {
		StringBuffer sb = new StringBuffer();
		int count=1;
		sb.append(String.format("  %2.2s   %-60.45s   %-10.10s   %10.10s   %10.10s   %-3.3s   %-3.3s  ", 
				                 "�", "����� ��������", "������", "����� ����", "����� ����", "���", "���")).append('\n');
		for(String[] shop : Shops.values()){
			String[] name = shop[0].split("(\\s*\\\"\\s*)|(\\s*\\�)|(\\�\\s*)");
			sb.append(String.format("| %2d | %-12.12s %-47.47s | %-10.10s | %10.10s | %10.10s | %-3.3s | %-3.3s |", 
					                 count++, name[1], name[2],   shop[1],  shop[2],  shop[3], shop[4], shop[5])).append('\n');
		}
		sb.append(ended);
		Screen.replace(sb.toString());
	}

	private void buildMapShops(){
		Screen.echo("������� �� �������� ��������");
		Shops = new HashMap<String, String[]>();
		getPageHTML("report/?report=balance_sheet");
		Screen.echoIn("���������� ������ ���������. ");
		Element shops_select = document.htmlDoc.getElementById("shops");
		for(Node group : shops_select.childNodes())
			if(group.childNodeSize() > 0)
				for(Node option : group.childNodes())
					if(!option.attr("value").isEmpty())
						Shops.put(option.attr("value"), new String[]{option.childNode(0).outerHtml().substring(2).trim(), "��������", "0.00", "0.00", "0", "0"});
		Screen.echo(" ������� " + Shops.size());
	}
	
	private void getRestPage(String shop_rid) throws IOException {
		StringBuffer request = new StringBuffer();
		request.append("SORT_FIELD=NAME&SORT_TYPE=ASC&")
		       .append("DAT"
		       		+ "E_FROM=").append(Formatter_2.format(bdate))
		       .append("&DATE_TO=").append(Formatter_2.format(edate))
		       .append("&SHOPS%5B%5D=").append(shop_rid)
		       .append("&submit=%CE%F2%EF%F0%E0%E2%E8%F2%FC");
		
		//Timer timer = new Timer(Thread.currentThread());
		postPageHTML("report/?report=balance_sheet", request.toString());
		//timer.setEnd();	
		
		addShopWithHTMLDocument(document.htmlDoc, shop_rid);
				
	}
	
	private static void addShopWithHTMLDocument(Document document, String shop_rid) throws IOException {
		int count=0;
		int count_empty=0;
		double enter_sum=0;
		double sell_sum=0;
		
		Shops.get(shop_rid)[1] = "��������";
		
		if(document.getElementsByClass("balance_sheet").size()==0 ||
		   document.getElementsByClass("report").size()==0){
			Shops.get(shop_rid)[1] = "������";
			Screen.echo("timeout");
			return;
		}
		
		String shop_id = document.getElementsByClass("balance_sheet").get(0).childNode(3).childNode(1).outerHtml().split("(\\()|(\\))")[1];		
		Element table = document.getElementsByClass("report").get(0);		
		Node tbody = table.child(1);
		
tr:		for(Node tr : tbody.childNodes()){
			if(tr instanceof TextNode)
				continue;
			String[] row = new String[30];
			row[0] = shop_id;
			row[1] = Shops.get(shop_rid)[0];
			row[2] = "";

			int i=3;
			for(Node td : tr.childNodes())
				if(!(td instanceof TextNode) && 
				     td.childNodeSize()>0){
					if(td.childNode(0).outerHtml().equals("C����"))
						continue tr;
					row[i++] = td.childNode(0).outerHtml();
				}	
			try(FileWriter wr = new FileWriter(restFile, true)) {
				for (i = 7; i <= 14; i++) {
					if (row[i] != null && !row[i].equals("0.00")) {
						for(int j=0;j<row.length;j++) {
							wr.write("\"" + (row[j]!=null?row[j]:"") + (isNumeric(row[j])?"0000":"") + "\"" );
							wr.write(";");
						}
						wr.write('\n');
//						Excel.addRestRow(row);
						enter_sum += Double.parseDouble(row[10]);
						sell_sum += Double.parseDouble(row[12]);
						count++;
						break;
					} else
						count_empty++;
				}
			}
													
		}
		Shops.get(shop_rid)[1] = "������";
		Shops.get(shop_rid)[2] = String.format("%7.2f", enter_sum);
		Shops.get(shop_rid)[3] = String.format("%7.2f", sell_sum);
		Shops.get(shop_rid)[4] = Integer.toString(count);
		Shops.get(shop_rid)[5] = Integer.toString(count_empty/7);
	}

	private static boolean isNumeric(String str) {
		return str != null ? str.matches("-?\\d+(\\.\\d+)?") : false;
	}
}
