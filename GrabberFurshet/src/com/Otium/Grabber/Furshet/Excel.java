//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.Otium.Grabber.Furshet;

import java.io.File;
import java.io.FileOutputStream;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Excel {
    private static final DateTimeFormatter Formatter_3 = DateTimeFormatter.ofPattern("yyyy_MM_dd");
    private static int focus_row;
    private static XSSFWorkbook WorkBook;
    private static XSSFSheet sheet;

    public Excel() {
    }

    protected static void openSchameFileRests() {
        focus_row = 1;

        try {
            Timer timer = new Timer(Thread.currentThread(), Furshet.Screen);
            Furshet.Screen.echoIn("�������� Excel �������");
            WorkBook = new XSSFWorkbook(ClassLoader.getSystemResourceAsStream("������� ������.xlsx"));
            sheet = WorkBook.getSheet("rest_gen");
            timer.setEnd();
            Furshet.Screen.echo("������");
        } catch (Exception var1) {
            Furshet.Screen.echo("������");
            Furshet.Screen.echo("��������� ��������� : " + var1.getMessage());
        }

    }

    protected static void saveFileRests() {
        try {
            Furshet.Screen.echoIn("���������� Excel �����");
            Timer timer = new Timer(Thread.currentThread(), Furshet.Screen);
            FileOutputStream out = new FileOutputStream(new File("������� " + Formatter_3.format(LocalDate.now()) + ".xlsx"));
            WorkBook.write(out);
            out.close();
            WorkBook.close();
            timer.setEnd();
            Furshet.Screen.echo("C�������, ��� ����� - '������� " + Formatter_3.format(LocalDate.now()) + ".xlsx'");
        } catch (Exception var2) {
            Furshet.Screen.echo("������");
            Furshet.Screen.echo("��������� ��������� : " + var2.getMessage());
        }

    }

    protected static void addRestRow(String[] data) {
        Row row = sheet.createRow(focus_row++);

        for(int i = 0; i < data.length; ++i) {
            if (isNumeric(data[i])) {
                row.createCell(i).setCellValue(Double.parseDouble(data[i]));
            } else {
                row.createCell(i).setCellValue(data[i]);
            }
        }

    }

    public static boolean isNumeric(String str) {
        return str != null ? str.matches("-?\\d+(\\.\\d+)?") : false;
    }
}
