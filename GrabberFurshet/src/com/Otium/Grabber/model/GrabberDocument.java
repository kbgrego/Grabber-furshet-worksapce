package com.Otium.Grabber.model;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class GrabberDocument {
	public Document htmlDoc;
	
	Document convertStringToDocument(String xmlStr) {
    	htmlDoc = Jsoup.parse(xmlStr);
		return htmlDoc;
    }

	public void setDocument(String pageHTML) {
		convertStringToDocument(pageHTML);		
	}
}
