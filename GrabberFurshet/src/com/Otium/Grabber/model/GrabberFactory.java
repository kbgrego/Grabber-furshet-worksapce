package com.Otium.Grabber.model;

import org.jsoup.nodes.Document;

import java.net.MalformedURLException;
import java.net.URL;

public abstract class GrabberFactory {
	protected URL base_url;
	protected Connector connector;
	protected GrabberDocument document;
	
	protected GrabberFactory(){		
		
	}
	
	public GrabberFactory(String url) throws MalformedURLException {		
		this.base_url = new URL(url);
		this.connector = new Connector();
		this.document = new GrabberDocument();
		Screen.printLogo("Logo.lg");
		//Screen.echo("Grabber initialized with name The " + this.getClass().getSimpleName() + " for " + url);
	}
	
	protected Document postPageHTML(String path, String params){	
		try {
			document.setDocument(connector.tryPostHTMLPage(base_url.toString() + path, params));
			//Screen.echo(document.htmlDoc.title());
		} catch (Exception e) {
			Screen.echo("FAIL!!!");
			Screen.echoIn("message : "); Screen.echo(e.getMessage());
		}
		return document.htmlDoc;
	}

	protected Document getPageHTML(String path) {		
		try {
			document.setDocument(connector.tryGetHTMLPage(base_url.toString() + path));
			//Screen.echo("Got page : " + document.htmlDoc.title());
		} catch (Exception e) {
			Screen.echo("FAIL!!!");
			Screen.echoIn("error message : "); Screen.echo(e.getMessage());
		}
		return document.htmlDoc;
	}
	
	protected byte[] getFileByteArray(String path){
		try {
			return connector.tryGetFile(base_url.toString() + path);
			//Screen.echo("Got page : " + document.htmlDoc.title());
		} catch (Exception e) {
			Screen.echo("FAIL!!!");
			Screen.echoIn("error message : "); Screen.echo(e.getClass().getName() + ": "); Screen.echo(e.getMessage());
		}		
		return new byte[]{};
	}

	
}
