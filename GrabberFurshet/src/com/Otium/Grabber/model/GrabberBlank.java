package com.Otium.Grabber.model;

import com.Otium.Grabber.model.Connector.Cookie;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Map;

public class GrabberBlank extends GrabberFactory {

	public GrabberBlank(String url) throws MalformedURLException{
		this.base_url = new URL(url);
		this.connector = new Connector();
		this.document = new GrabberDocument();
	}
	
	public Document getDocument(String path){
		return getPageHTML(path);
	}

	public byte[] getFile(String path) {
		return getFileByteArray(path);
	}

	public Document tryPostHTML(String path, Map<String, String> headers, String params) throws IOException {				
		document.setDocument(connector.tryPostHTMLPage(base_url.toString() + path, headers, params));
		return document.htmlDoc;
	}

	public List<Cookie> getCookies() {
		return Connector.Cookie.getCookies();		
	}

	public Document tryPostHTML(String path, String params) throws IOException {
		document.setDocument(connector.tryPostHTMLPage(base_url.toString() + path, params));
		return document.htmlDoc;
	}

	public Document tryGetHTML(String path) throws IOException {
		document.setDocument(connector.tryGetHTMLPage(base_url.toString() + path));
		return document.htmlDoc;
	}

}
