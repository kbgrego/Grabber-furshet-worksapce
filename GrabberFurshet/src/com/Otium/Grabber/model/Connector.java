package com.Otium.Grabber.model;

import org.apache.poi.util.IOUtils;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.X509TrustManager;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Connector {
	private static final String COOKIE = "Cookie";
	private static final String SET_COOKIE = "Set-Cookie";

	static {
		X509TrustManager[] trustAllCerts = new X509TrustManager[]{
				new X509TrustManager() {
				    public X509Certificate[] getAcceptedIssuers() {
				        return null;
				    }
				    public void checkClientTrusted(
				    		X509Certificate[] certs, String authType) {
				    }
				    public void checkServerTrusted(
				    		X509Certificate[] certs, String authType) {
				    }
				}};

				   try {
				    SSLContext sc = SSLContext.getInstance("SSL");
				    sc.init(null, trustAllCerts, new java.security.SecureRandom());
				    HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
				    } catch (Exception e) {
				    }
	}
	
	private URLConnection connection;
		
	String tryGetHTMLPage(String path) throws IOException{
	   //Screen.echo("--- GET REQUEST TO '" + path.toString() + "' ---");	
	   URL url = new URL(path);
	   String charset=null;
	   connection = url.openConnection();
	   ((HttpURLConnection) connection).setAllowUserInteraction(true);
	   setCookies(connection);

		if(connection.getContentType().indexOf("charset=")>0){
			charset = connection.getContentType().substring(connection.getContentType().indexOf("charset=")+8);
			//Screen.echo("|Set charset: " + charset);
		}

	   BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream(), charset));
	   String input;
	   StringBuffer result = new StringBuffer();
	   //Screen.echoIn("|Status "); 
	   //Screen.echoIn(Integer.toString(((HttpURLConnection) connection).getResponseCode()));
	   //Screen.echo(" " + ((HttpURLConnection) connection).getResponseMessage());

	   while ((input = br.readLine()) != null){
		   result.append(input);
	   }	     
	   //Screen.echoIn("|Recived " + result.length());
	   br.close();
	   //Screen.echo(" ...success!");

	   initCookies(connection);
	   //Screen.echo("--- END ---------- '" + path.toString() + "' ---");	
	   return result.toString();   	   
	}
	
	byte[] tryGetFile(String path) throws IOException {
		   //Screen.echo("--- GET FILE FROM  '" + path.toString() + "' ---");	
		   connection = new URL(path).openConnection();
		   setCookies(connection);
		   //Screen.echoIn("|Status "); 
		   //Screen.echoIn(Integer.toString(((HttpURLConnection) connection).getResponseCode()));
		   //Screen.echo(" " + ((HttpURLConnection) connection).getResponseMessage());
			    			   
		   initCookies(connection);
		   
		   byte[] input = IOUtils.toByteArray(connection.getInputStream());
		   
		   //Screen.echoIn("|Recived " + input.length);
		   //Screen.echo(" ...success!");
		   //Screen.echo("--- END ---------- '" + path.toString() + "' ---");
		   return input;	    
	}

	
	String tryPostHTMLPage(String path, String params) throws IOException{
		return tryPostHTMLPage(path, null, params);
	}
	
	
	private void setCookies(URLConnection connection){
		StringBuffer buf = new StringBuffer();
		for(Cookie cookie : Cookie.list)
			buf.append(cookie.key).append("=").append(cookie.value).append(";");
		connection.setRequestProperty(COOKIE, buf.toString());
	}
	
	private void initCookies(URLConnection connection){
		String headerName;
		for (int i=1; (headerName = connection.getHeaderFieldKey(i))!=null; i++) {
			if (headerName.equals(SET_COOKIE)) {  
				String cookie = connection.getHeaderField(i);
				cookie = cookie.substring(0, cookie.indexOf(";"));
				new Cookie(cookie.substring(0, cookie.indexOf("=")), cookie.substring(cookie.indexOf("=") + 1, cookie.length()));
			}
		}		
	}
	
	public static class Cookie{
		protected static List<Cookie> list = new ArrayList<>();
		public static List<Cookie> getCookies(){
			return list;			
		}
		
		public String key;
		public String value;
		public Cookie(String k, String v) {
			key=k;value=v;						
			list.removeIf(c -> c.key.equals(k));
			if(v!=null&&!v.isEmpty()&&!v.equals("null"))
				list.add(this);

			//Screen.echo("|Set cookie : " + k + " = " + v);			
		}
		
		@Override
		public String toString(){
			return key + "=" + value;
		}

		public static boolean has(String string) {
			for(Cookie c : list)
				if(c.key.equals(string))
					return true;
			return false;
		}
	}

	public String tryPostHTMLPage(String path, Map<String, String> headers, String params) throws IOException {
		   //Screen.echo("--- POST REQUEST TO '" + path.toString() + "' ---");
		   String charset=null;
		   URL url = new URL(path);
		   connection = url.openConnection();
		   connection.setReadTimeout(30000);
		   ((HttpURLConnection) connection).setRequestMethod("POST");
		   setCookies(connection);
		   ((HttpURLConnection) connection).setInstanceFollowRedirects(false);
		   if(headers!=null)
			   headers.forEach((key,value) -> connection.setRequestProperty(key,value));
		   //Screen.echo("|Host: " + connection.getRequestProperty("Host"));
		   //connection.getRequestProperties().forEach((key, value) -> Screen.echo("|" + key + ": " + value ));
		   connection.setDoOutput(true);		   
		   DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
		   wr.writeBytes(params);
		   wr.flush();
		   wr.close();
		   //Screen.echo("|Posted " + params.length());


		if(connection.getContentType().indexOf("charset=") > 0){
			charset = connection.getContentType().substring(connection.getContentType().indexOf("charset=") + 8);
			//Screen.echo("|Set charset: " + charset);
		}

		BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream(), charset));
		   String input;
		   StringBuffer result = new StringBuffer();
		   
		   /*
		   Screen.echoIn("|Status "); 
		   Screen.echoIn(Integer.toString(((HttpURLConnection) connection).getResponseCode()));
		   Screen.echo(" " + ((HttpURLConnection) connection).getResponseMessage());
		   if(((HttpURLConnection) connection).getResponseCode() == 303 )
			   Screen.echo("|Location " + connection.getHeaderField("Location"));
		   */

		   while ((input = br.readLine()) != null) {
			   result.append(input);
		   }

		   //Screen.echoIn("|Recived " + result.length());
		   br.close();
		   //Screen.echo(" ...success!");
		   
		   //connection.getHeaderFields().forEach((key, value) -> Screen.echo("|" + key + ": " + value ));

		   initCookies(connection);		   
		   //Screen.echo("--- END ----------- '" + path.toString() + "' ---");
		   return result.toString();	
	}
}
