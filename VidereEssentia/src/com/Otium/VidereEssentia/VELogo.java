package com.Otium.VidereEssentia;

import com.Otium.VidereEssentia.controls.Logo;

class VELogo extends Logo{
	private final static String[][] VE_LOGO = {{" _     __                ____                       ", LOGO_STYLE},
											   {" \\    //    _| _     _   ||    _  _  _   _   |   _  ", LOGO_STYLE},
											   {"  \\  //  | | ||_ |/ |_   ||-- |_ |_ |_ |/ | -|- |_| ", LOGO_STYLE},
											   {"   \\//   | |_||_ |  |_   ||__  _| _||_ |  |  |/ | | ", LOGO_STYLE},
											   {"   /\\\\   | | ||  |  |    ||     |  ||  |  |  |\\ | | ", DESC_STYLE}};
	private final static String[] VE_DESC = {" ","                              by Otium Group (2016) "};
	
	@Override
	public String[][] getLogoStrings() {
		return VE_LOGO;
	}
	
	@Override
	public String[] getDescStrings() {
		return VE_DESC;
	}
	
	
}
