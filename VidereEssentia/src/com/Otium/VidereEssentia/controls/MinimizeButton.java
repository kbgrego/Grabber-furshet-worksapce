package com.Otium.VidereEssentia.controls;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

@SuppressWarnings("serial")
public class MinimizeButton extends JButton implements StylerConsole {
	
	public MinimizeButton(int width){
        setText("Minimize");
        setMinimumSize(new Dimension(80, 25));
        setBackground(TOPPANE_color);
        setBorderPainted(false);
        setFocusPainted(false);
        setContentAreaFilled(false);
        setForeground(FORE_color);
		setOpaque(true);
        setFont(new Font(FONT_style, Font.PLAIN, FONT_size));
        
        MinimizeButton button = this;
        addActionListener((action) -> {
        	JFrame control = (JFrame) SwingUtilities.getRoot(button);
        	control.setState(Frame.ICONIFIED);        	
        });
        
        MinimizeButton jButton = this;
        addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jButton.setForeground(Color.WHITE);
                jButton.setBackground(TOPPANE_HOVER_color);
            }

            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButton.setForeground(FORE_color);
                jButton.setBackground(TOPPANE_color);
            }
        });
	}

}
