package com.Otium.VidereEssentia.controls;

import java.awt.Color;

import javax.swing.text.BadLocationException;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

public abstract class Logo implements StylerConsole, ConsoleView {
	protected static final String LOGO_STYLE = "logo";
	protected static final String DESC_STYLE = "desc";
	private static Style  picture_style = null; 
	private static Style  description_style = null;
	
	public String[] nameStyles(){
		return new String[]{LOGO_STYLE, DESC_STYLE};
	}
	
	public void setStyle(Style style, int index){
		switch(index){
			case 0:
		        StyleConstants.setFontFamily(style, FONT_style); 
		        StyleConstants.setFontSize(style, FONT_size);
		        StyleConstants.setForeground(style, Color.white);
		        StyleConstants.setAlignment(style, StyleConstants.ALIGN_CENTER);
		        picture_style = style;
			break;
			case 1:
		        StyleConstants.setFontFamily(style, FONT_style); 
		        StyleConstants.setFontSize(style, FONT_size);
		        StyleConstants.setForeground(style, new Color(120,120,120));
		        StyleConstants.setAlignment(style, StyleConstants.ALIGN_CENTER);
		        description_style = style;
			break;
		}
	}
	
	public void showIn(StyledDocument doc){	
		String[][] LOGO = getLogoStrings();
		String[] DESC = getDescStrings();
		try {
			doc.setParagraphAttributes(0, doc.getLength(), picture_style, false);
			
			for(int i=0; i<LOGO.length+5; i++)
				doc.insertString(0, "\n", picture_style);
			int start = doc.getLength()-LOGO.length;
			for(int i=0;i<LOGO[0][0].length();i++){
				for(int j=0;j<LOGO.length;j++)
					doc.insertString(start + (j * 2) + (1 + j) * i, LOGO[j][0].charAt(i)+"", LOGO[j][1]==LOGO_STYLE ? picture_style : description_style);
				Thread.sleep(20);
			}
			doc.insertString(doc.getLength(), "\n", description_style);
			
			for(int i=0;i<DESC.length;i++)
				doc.insertString(doc.getLength(), DESC[i] + "\n", description_style);
		} catch (BadLocationException | InterruptedException e) {
			e.printStackTrace();
		}
	}

	public abstract String[][] getLogoStrings();
	public abstract String[] getDescStrings();	
}
