package com.Otium.VidereEssentia.controls;

import java.awt.Color;

import com.sun.media.jfxmedia.events.NewFrameEvent;

public interface StylerConsole {
	static final String STYLE_heading = "heading"
			           ,STYLE_normal  = "normal" 
			           ,FONT_style    = "Lucida Console"
			           ;
    static final int    FONT_size = 14;
	static final Color  FORE_color = new Color(200,200,210)
			           ,GROUND_color = new Color(50,50,50)
			           ,TOPPANE_color = new Color(80,80,80)
			           ,TOPPANE_HOVER_color = new Color(100,100,100)
			           ,TOPPANE_Click_color = new Color(120,120,120)
					   ;
}
