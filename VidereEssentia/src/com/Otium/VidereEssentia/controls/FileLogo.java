package com.Otium.VidereEssentia.controls;

import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class FileLogo extends Logo {
	private final String PATH;
	public FileLogo(String path){
		PATH = path;
	}

	@Override
	public String[][] getLogoStrings() {
		List<String> list = new ArrayList<>();
		try {
			Scanner reader = new Scanner(new InputStreamReader(ClassLoader.getSystemResourceAsStream(PATH)));
			while(reader.hasNextLine())
				list.add(reader.nextLine());
			reader.close();
		} catch (Exception e) {
			System.out.print("Logo is missing.");
			e.printStackTrace();
		} 
		System.out.print('\n');		
		String[][] logo = new String[list.size()][2];

		for(int i=0;i<list.size();i++){
			logo[i][0]=list.get(i); 
			logo[i][1]=LOGO_STYLE;
		};
		return logo;
	}

	@Override
	public String[] getDescStrings() {
		return new String[]{};
	}

}
