package com.Otium.Grabber.model;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Screen {
	public static void echo(String text){
		System.out.println(text);
	}
	
	public static void echoIn(String text){
		System.out.print(text);
	}
	
	public static void printLogo(String path) {			
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(ClassLoader.getSystemResourceAsStream(path)));
			while(reader.ready())
				System.out.print((char)reader.read());
			
		} catch (Exception e) {
			System.out.print("Logo is missing.");
			e.printStackTrace();
		} 
		System.out.print('\n');
	}
}
