package com.Otium.Grabber.model;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class OutputScreen {
	OutputStream boss;
	JTextArea console;
	JFrame frame;
	

	public OutputScreen(String title) {
        frame = new JFrame();
        frame.setTitle(title);
        frame.setSize(600,650);             

        console = new JTextArea(40,100); 
        
        console.setBackground(new Color(0,0,0,255));       
        console.setForeground(Color.lightGray);
        console.setFont(new Font("Lucida Console", Font.PLAIN, 12));
        console.setCaretColor(Color.WHITE);
        console.setEditable(false);
        
        boss = new BufferedOutputStream(new CustomOutputStream(console));        

        frame.add( new JScrollPane( console )  );

        frame.pack();
        frame.setVisible( true );        
	}
	
	public void setImage(Image image){
		frame.setIconImage(image);
	}
	
	PrintStream getPrintStream(){
		return new PrintStream(boss);
	}
	
	private static class CustomOutputStream extends OutputStream {
	    private JTextArea textArea;
	     
	    public CustomOutputStream(JTextArea textArea) {
	        this.textArea = textArea;
	    }
	     
	    @Override
	    public void write(int b) throws IOException {
	        // redirects data to the text area
	        textArea.append(new String(new byte[]{(byte)b}));
	        // scrolls the text area to the end of data
	        textArea.setCaretPosition(textArea.getDocument().getLength());
	    }
	}

	public void echo(String text){
		echoIn(text);
		echoIn('\n');
	}
	
	void echoIn(char c) {
		try {
			boss.write(c);
			boss.flush();
		} catch (IOException e) {

		}
	}

	public void echoIn(String text){
		try {
			boss.write(text.getBytes());
			boss.flush();
		} catch (IOException e) {

		}
	}

	public void clear() {
		console.setText("");
	}
	
	public void replace(String text) {
		console.setText(text);
	}
	
	/*
	int posLastLine;
	String LastLine;
	public String readLine(){
		posLastLine = console.getLineCount()-1;
		try {
			LastLine = console.getText(console.getLineStartOffset(posLastLine), console.getLineEndOffset(posLastLine));
		} catch (BadLocationException e) {
		}
        console.getDocument().addDocumentListener(getDocumentListner());
        console.addCaretListener(getCaretListner());
        console.setEditable(true);

        return console.getText(offs, len);
	}
	
	private CaretListener getCaretListner() {

		return  new CaretListener() {
			@Override
		      public void caretUpdate(CaretEvent caretEvent) {
				if(caretEvent.getDot()!=caretEvent.getMark())
					console.setSelectionStart(caretEvent.getDot());
	            try {
		          if(console.getLineOfOffset(caretEvent.getDot())!=console.getLineCount()-1)
		      			console.moveCaretPosition(console.getLineEndOffset(console.getLineCount()-1));
	      		} catch (BadLocationException e) {
	      			e.printStackTrace();  	        	  
		        }
			}
		};
	}

	private DocumentListener getDocumentListner(){
		return new DocumentListener() {

            @Override
            public void removeUpdate(DocumentEvent e) {
            	//System.out.println(console.getText());
            }

            @Override
            public void insertUpdate(DocumentEvent e) {
            	//System.out.println(console.getText());
            }

            @Override
            public void changedUpdate(DocumentEvent arg0) {
            	//System.out.println(console.getText());
            }
        };
	}
	*/
}
